package com.sparsetech.sentrypharmacies4ankara;

import android.util.Log;

import androidx.annotation.NonNull;

import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.Date;

public class Utils {
    public static void ParsePharmacyModel(@NonNull ArrayList<PharmacyModel> models, final @NonNull String html) {
        Document doc = Jsoup.parse(html);

        Elements listInfo = doc.getElementsByClass("list-group");
        for (Element row : listInfo) {
            for (Element e : row.getElementsByTag("span")) {
                String info = e.toString().replaceAll("<br>", "::").replaceAll("<span>", "").replaceAll("</span>", "").trim();
                String[] a = info.split("::");
                if (a.length != 4) continue;
                PharmacyModel eczaneModel = new PharmacyModel(a[0].trim(), a[1].trim() + "\n" + a[0].trim(), null);
                models.add(eczaneModel);
                Log.i("debug", info);
            }
        }

        Elements list = doc.getElementsByTag("script");
        for (Element e : list) {
            for (DataNode d : e.dataNodes()) {
                String s = d.getWholeData();
                if (!s.contains("locations")) continue;

                String[] l = s.split("\n");
                for (String a : l) {
                    if (!a.contains("href")) continue;
                    a = a.trim();
                    int i = a.lastIndexOf('[');
                    if (i > 0)
                        a = a.substring(i);

                    a = a.substring(1, a.indexOf(']')).replaceAll(",", "::").replaceAll("'", "");
                    String[] v = a.split("::");
                    if (v.length != 4) continue;

                    for (PharmacyModel m : models)
                        if (m.name.equals(v[0])) {
                            String sdf = v[1].replaceAll("\\(", "").replaceAll("\\)", "").trim();
                            String sdf1 = v[2].replaceAll("\\(", "").replaceAll("\\)", "").trim();
                            m.startPoint = new GeoPoint(Double.parseDouble(sdf), Double.parseDouble(sdf1));
                        }

                    Log.i("debug", a);
                }
            }
        }

    }

    public static String getNowDate() {
        Date d = new Date();
        CharSequence s = android.text.format.DateFormat.format("dd.MM.yyyy", d.getTime());

        return s.toString();
    }
}
