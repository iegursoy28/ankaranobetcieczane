package com.sparsetech.sentrypharmacies4ankara;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.commons.text.StringEscapeUtils;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private static int MY_PERMISSIONS_REQUEST_LOCATION = 2828;

    private boolean isRefreshing = false;

    @BindView(R.id.main_map)
    public MapView map;

    @BindView(R.id.main_my_location_btn)
    public ImageButton myLocationBtn;

    @BindView(R.id.toolbar_refresh_btn)
    public ImageButton refreshBtn;

    @BindView(R.id.toolbar_title)
    public TextView toolbarTextView;

    @OnClick(R.id.main_my_location_btn)
    public void onClickMyLocationBtn(View v) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);

            return;
        }

        if (myLocationOverlay == null) {
            // My Location Overlay
            myLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(ctx), map);
            myLocationOverlay.enableMyLocation();
            map.getOverlays().add(myLocationOverlay);
        }

        map.getController().animateTo(myLocationOverlay.getMyLocation());
    }

    @OnClick(R.id.toolbar_refresh_btn)
    public void onClickRefreshBtn(View v) {
        if (isRefreshing)
            return;

        RotateAnimation rotate = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        rotate.setDuration(1000);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setRepeatCount(Animation.INFINITE);

        refreshBtn.startAnimation(rotate);
        isRefreshing = true;
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isRefreshing) {
                    Toast.makeText(ctx, "Üzgünüz listeyi güncelleyemedik", Toast.LENGTH_SHORT).show();
                    refreshBtn.clearAnimation();
                    isRefreshing = false;
                }
            }
        }, 5000);

        map.getOverlayManager().overlays().clear();
        map.invalidate();
        GetList();
    }

    private Context ctx;
    private RequestQueue requestQueue;
    private MyLocationNewOverlay myLocationOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = this.getApplicationContext();

        // Setup map
        Configuration.getInstance().load(ctx,
                PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        toolbarTextView.setText(String.format("%s\n(%s)", getString(R.string.app_name), Utils.getNowDate()));

        // Setup UI
        initUI();

        // Volley setup
        requestQueue = Volley.newRequestQueue(ctx);
    }

    public void initUI() {
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);
        map.getController().setZoom(Constants.MAP_ZOOM);
        map.getController().animateTo(Constants.ANKARA_LOCATION);
        map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT);
    }

    public void onResume() {
        super.onResume();
        map.onResume();
        onClickRefreshBtn(null);
    }

    public void onPause() {
        super.onPause();
        map.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onClickMyLocationBtn(null);
            } else {
                Toast.makeText(ctx, "Konum izni gereklidir", Toast.LENGTH_SHORT).show();
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void GetList() {
        String b = String.format("NobetTarihiAsString=%s&IlceKey=00000000-0000-0000-0000-000000000000", Utils.getNowDate());

        StringRequest request = new StringRequest(Request.Method.GET, "http://www.aeo.org.tr/NobetModulu/Nobet?" + b,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        response = StringEscapeUtils.unescapeHtml4(response);

                        ArrayList<PharmacyModel> eczaneModels = new ArrayList<>();
                        Utils.ParsePharmacyModel(eczaneModels, response);

                        for (PharmacyModel e : eczaneModels)
                            AddMarker(map, e);

                        map.invalidate();

                        refreshBtn.clearAnimation();
                        isRefreshing = false;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "İnternete bağlanılamıyor... Lütfen bağlantınızı kontrol edin!";
                        } else if (volleyError instanceof ServerError) {
                            message = "Sunucu bulunamadı. Lütfen bir süre sonra tekrar deneyin!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "İnternete bağlanılamıyor... Lütfen bağlantınızı kontrol edin!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Ayrıştırma hatası! Lütfen bir süre sonra tekrar deneyin!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Bağlantı zamanaşımı! Lütfen internet bağlantınızı kontrol edin.";
                        } else {
                            message = volleyError.toString();
                        }

                        Log.e("debug", message);
                        String finalMessage = message;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ctx, finalMessage, Toast.LENGTH_SHORT).show();
                            }
                        });

                        refreshBtn.clearAnimation();
                        isRefreshing = false;
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("cookie", "AspxAutoDetectCookieSupport=1");
                map.put("content-type", "application/x-www-form-urlencoded");

                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                4000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(request);
    }


    public synchronized void Redirect2GoogleMap(@NonNull PharmacyModel model) {
        String url = String.format("http://www.google.com/maps/place/%s,%s", model.startPoint.getLatitude(), model.startPoint.getLongitude());
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public synchronized View CreateInfoWindow(@NonNull final PharmacyModel model) {
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View view = inflater.inflate(R.layout.view_marker_info, null, false);

        ((TextView) view.findViewById(R.id.marker_info_title)).setText(String.format("%s %s", model.name, "Eczanesi"));
        ((TextView) view.findViewById(R.id.marker_info_address)).setText(model.address);
        ((Button) view.findViewById(R.id.marker_info_hide)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view.getVisibility() == View.VISIBLE)
                    view.setVisibility(View.GONE);
            }
        });
        ((ImageButton) view.findViewById(R.id.marker_info_map)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Redirect2GoogleMap(model);
            }
        });

        return view;
    }

    public synchronized void AddMarker(@NonNull final MapView view, @NonNull PharmacyModel model) {
        if (model.startPoint == null) return;

        Marker startMarker = new Marker(view);
        if (Build.VERSION.SDK_INT >= 23)
            startMarker.setIcon(getDrawable(R.drawable.custom_marker));
        startMarker.setPosition(model.startPoint);
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);

        PharmacyInfoWindow infoWindow = new PharmacyInfoWindow(CreateInfoWindow(model), map);
        startMarker.setInfoWindow(infoWindow);

        view.getOverlays().add(startMarker);
    }
}
