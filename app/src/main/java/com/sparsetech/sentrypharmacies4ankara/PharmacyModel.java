package com.sparsetech.sentrypharmacies4ankara;

import org.osmdroid.util.GeoPoint;

public class PharmacyModel {
    public String name;
    public String address;
    public GeoPoint startPoint;

    public PharmacyModel(String name, String address, GeoPoint startPoint) {
        this.name = name;
        this.address = address;
        this.startPoint = startPoint;
    }
}
