package com.sparsetech.sentrypharmacies4ankara;

import org.osmdroid.util.GeoPoint;

public class Constants {
    public static double MAP_ZOOM = 12.0;
    public static GeoPoint ANKARA_LOCATION = new GeoPoint(39.907443, 32.750808); // Ankara location
}
