package com.sparsetech.sentrypharmacies4ankara;

import android.view.View;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

public class PharmacyInfoWindow extends InfoWindow {
    private View view;

    public PharmacyInfoWindow(View v, MapView m) {
        super(v, m);
        view = v;
    }

    @Override
    public void onOpen(Object item) {
        if (view != null && view.getVisibility() != View.VISIBLE)
            view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClose() {
    }
}
